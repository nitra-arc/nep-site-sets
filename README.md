# SetsBundle

## Описание
Выводит набор товаров, которые можно приобрести вместе с просматриваемым товаром.

Данный бандл предназначен для работы (вывода, обработки) с:

* сетами (Sets) - вывод сета товаров (определяется в админке)
## SetsConstroller
* indexAction - возвращает сет товаров по выбранному товару
* getSetProducts - функция используется indexAction, возвращает сет товаров по выбранному товару

## Подключение
Для подключения данного модуля в проект необходимо добавить:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        "e-commerce-site/setsbundle": "1.2",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\SetsBundle\NitraSetsBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...

```