<?php

namespace Nitra\SetsBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\StoreBundle\Helper\nlActions;

class SetsController extends NitraController
{
    /**
     * @Template("NitraSetsBundle:Sets:index.html.twig")
     */
    public function indexAction($productId, $withExchange = true)
    {
        $sets        = $this->getSets($productId);
        $productSets = array();
        foreach ($sets as $key => $set) {
            $setProducts = $this->getSetProducts($set, $productId, $withExchange);
            if (key_exists('setEntries', $setProducts)) {
                $productSets[$key]         = $setProducts;
                $productSets[$key]['name'] = $set->getName();
                $productSets[$key]['id']   = $set->getId();
            }
        }

        return array(
            'sets' => $productSets,
        );
    }

    /**
     * @Template("NitraSetsBundle:Sets:allSets.html.twig")
     */
    public function allSetsAction($withExchange = true)
    {
        $allSets    = $this->getSets();
        $sets       = array();

        foreach ($allSets as $key => $set) {
            $setProducts = $this->getSetProducts($set, null, $withExchange);
            if (key_exists('setEntries', $setProducts)) {
                $sets[$key]         = $setProducts;
                $sets[$key]['name'] = $set->getName();
                $sets[$key]['id']   = $set->getId();
            }
        }
        return array(
            'sets' => $sets,
        );
    }

    /**
     * @param \Nitra\SetsBundle\Document\Sets   $set
     * @param string                            $productId
     * @param boolean                           $withExchange
     * @return array
     */
    protected function getSetProducts($set, $productId, $withExchange)
    {
        $productSets = array();
        foreach ($set->getSetEntries() as $key => $entrie) {
            switch ($entrie->getType()) {
                case "product":
                    $product        = $entrie->getProduct();
                    break;
                case "category":
                    $product        = $this->getDocumentManager()->find('NitraProductBundle:Product', $productId);
                    break;
            }
            $actionDiscount = nlActions::getProductDiscount($product);
            $discount       = ($actionDiscount > $entrie->getDiscount()) ? $actionDiscount : $entrie->getDiscount();

            if (($product->getId() == $productId || null === $productId) && $entrie->getMain()) {
                $productSets['mainProduct']     = $product;
                $productSets['mainDiscount']    = $discount;
                $productSets['price']           = $this->calckEntryPrice($product, $discount, $withExchange);
            } else {
                $productSets['setEntries'][$key]['product']     = $product;
                $productSets['setEntries'][$key]['discount']    = $discount;
                $productSets['setEntries'][$key]['price']       = $this->calckEntryPrice($product, $discount, $withExchange);
            }
        }

        return $productSets;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param int $discount
     * @param bool $withExchange
     * @return float
     */
    protected function calckEntryPrice($product, $discount, $withExchange)
    {
        $round          = Globals::getPriceRound();
        $originalPrice  = $product->getOriginalPrice();
        $exchange       = $withExchange
            ? Globals::getCurrency('exchange')
            : 1.0;
        $baseExchange   = $withExchange
            ? Globals::getBaseCurrency('exchange')
            : 1.0;

        $price          = round(($originalPrice * (100 - $discount)) / 100 * $baseExchange / $exchange, $round);

        return $price;
    }

    /**
     * @param string $productId
     * @return \Nitra\SetsBundle\Document\Sets[]
     */
    protected function getSets($productId)
    {
        $qb         = $this->getDocumentManager()->createQueryBuilder('NitraSetsBundle:Sets');
        $expr       = $qb->expr()->field('main')->equals(true);

        if ($productId) {
            $product = $this->getDocumentManager()->find('NitraProductBundle:Product', $productId);
            if ($product) {
                $expr->addOr($qb->expr()
                    ->field('product.$id')->equals(new \MongoId($productId))
                )->addOr($qb->expr()
                    ->field('category.$id')->equals(new \MongoId($product->getCategory()->getId()))
                );
            }
        }
        $qb->field('setEntries')->elemMatch($expr);

        $this->appendDateConditionsToQuery($qb);

        return $qb->getQuery()->execute();
    }

    /**
     * @param \Doctrine\ODM\MongoDB\Query\Builder $qb
     */
    protected function appendDateConditionsToQuery($qb)
    {
        $qb->addAnd($qb->expr()
            ->addOr($qb->expr()
                ->field('dateFrom')->lte(new \DateTime())
                ->field('dateTo')->gte(new \DateTime())
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->gte(new \DateTime())
            )->addOr($qb->expr()
                ->field('dateFrom')->lte(new \DateTime())
                ->field('dateTo')->exists(false)
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->exists(false)
            )
        );
    }
}