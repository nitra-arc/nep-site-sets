<?php

namespace Nitra\SetsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\EmbeddedDocument
 */
class SetEntry
{
    /**
     * @ODM\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * @ODM\String
     */
    protected $type;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Product")
     */
    protected $product;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $category;

    /**
     * Процент скидки
     * @ODM\Float
     * @Assert\Range(min = 0, max = 100)
     */
    protected $discount = 0;

    /**
     * @ODM\Boolean
     */
    protected $main;

    /**
     * Get id
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set product
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return self
     */
    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Get product
     * @return \Nitra\ProductBundle\Document\Product $product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return \Nitra\ProductBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set discount
     * @param float $discount
     * @return self
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * Get discount
     * @return float $discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set main
     * @param boolean $main
     * @return self
     */
    public function setMain($main)
    {
        $this->main = $main;
        return $this;
    }

    /**
     * Get main
     * @return boolean $main
     */
    public function getMain()
    {
        return $this->main;
    }
}